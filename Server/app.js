const settings = require(__dirname + '/settings.js');
const io = require('socket.io')(settings.port);

console.log('Server Started on 127.0.0.1:' + settings.port);

io.on('connection', function(socket){
	console.log( socket.id + ' Connected');
	socket.on('disconnect', function(data) {
		console.log( socket.id + ' Disconnected');
	});
  //io.emit('tweet', tweet);
});